package br.com.itau.Pagamento.Clients;

import br.com.itau.Pagamento.Decoder.CartaoClientDecoder;
import feign.codec.ErrorDecoder;
import org.springframework.context.annotation.Bean;

public class CartaoClientConfiguration {

    @Bean
    public ErrorDecoder getErrorDecoder(){
        return new CartaoClientDecoder();
    }

}
