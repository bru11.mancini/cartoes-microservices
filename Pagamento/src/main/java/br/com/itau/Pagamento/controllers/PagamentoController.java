package br.com.itau.Pagamento.controllers;

import br.com.itau.Pagamento.dto.PagamentoDTO;

import br.com.itau.Pagamento.models.Pagamento;
import br.com.itau.Pagamento.services.PagamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping()
public class PagamentoController {

    @Autowired
    private PagamentoService pagamentoService;



    @PostMapping("/pagamento/")
    @ResponseStatus(HttpStatus.CREATED)
    public Pagamento efetuarPagamento(@RequestBody PagamentoDTO pagamentoDTO){
        Pagamento pagamento = new Pagamento();
        pagamento.setDescricao(pagamentoDTO.getDescricao());
        pagamento.setValor(pagamentoDTO.getValor());
        return pagamentoService.efetuarPagamento(pagamentoDTO.getCartao_id(), pagamento);
    }

    @GetMapping("/pagamentos/{idCartao}")
    public List<Pagamento> exibirPagamentos (@PathVariable(name = "idCartao", required = false) Integer idCartao){

        try {
            List<Pagamento> pagamentos = pagamentoService.buscarPagamentoPorIdCartao(idCartao);
            return pagamentos;
        } catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

}
