package br.com.itau.Pagamento.repositories;

import br.com.itau.Pagamento.models.Pagamento;

import org.springframework.data.repository.CrudRepository;

public interface PagamentoRepository extends CrudRepository<Pagamento, Integer> {
    Iterable<Pagamento> findAllByidCartao(Integer idCartao);

}
