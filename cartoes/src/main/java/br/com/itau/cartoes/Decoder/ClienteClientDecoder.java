package br.com.itau.cartoes.Decoder;

import br.com.itau.cartoes.exceptions.ClienteNotFoudException;
import feign.Response;
import feign.codec.ErrorDecoder;


public class ClienteClientDecoder implements ErrorDecoder {

    private ErrorDecoder errorDecoder = new Default();

    @Override
    public Exception decode(String s, Response response){
        if (response.status() == 400){
            return new ClienteNotFoudException();
        }else{
            return errorDecoder.decode(s, response);
        }
    }
}
