package br.com.itau.cartoes.Clients;

import br.com.itau.cartoes.Decoder.ClienteClientDecoder;
import feign.codec.ErrorDecoder;
import org.springframework.context.annotation.Bean;

public class ClienteClientConfiguration {

    @Bean
    public ErrorDecoder getErroDecoder(){

        return new ClienteClientDecoder();
    }

}
