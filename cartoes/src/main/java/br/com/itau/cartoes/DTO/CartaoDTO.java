package br.com.itau.cartoes.DTO;

public class CartaoDTO {

    private int numero;

    private int clienteId;


    public CartaoDTO() {
    }

    public CartaoDTO(int clienteId) {
        this.clienteId = clienteId;
    }

    public int getClienteId() {
        return clienteId;
    }

    public void setClienteId(int clienteId) {
        this.clienteId = clienteId;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }
}
