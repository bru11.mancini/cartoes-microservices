package br.com.itau.cartoes.Clients;

import br.com.itau.cartoes.models.Cliente;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "Cliente", configuration = ClienteClientConfiguration.class)

public interface ClienteClient {

    @GetMapping("/cliente/{idCliente}")
    Cliente getClientePorId(@PathVariable int idCliente);
}
