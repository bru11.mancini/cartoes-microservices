package br.com.itau.cartoes.repositories;

import br.com.itau.cartoes.models.Cartao;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface CartaoRepository extends CrudRepository<Cartao, Integer> {
    Optional<Cartao> findAllByNumero(Integer numero);
}
